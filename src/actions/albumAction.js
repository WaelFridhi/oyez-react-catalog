import { FETCH_ALBUMS } from "./types";

export const fetchAlbums = () => dispatch => {
    console.log('fetching album');
    fetch('https://jsonplaceholder.typicode.com/albums')
        .then(res => res.json())
        .then(albums => dispatch({
            type: FETCH_ALBUMS,
            payload: albums
        }))
        .catch(error => console.warn(error));
};

const simpleAction = (payload) => ({
    type: '',
    payload
});
