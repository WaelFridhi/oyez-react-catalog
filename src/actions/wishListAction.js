import { ADD_FAVORITE } from "./types";

export const addToFavorite = picture =>
    ({
        type: ADD_FAVORITE,
        payload: picture
    });
