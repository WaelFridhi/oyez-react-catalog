import { FETCH_PICTURES } from "./types";

export const fetchPictures = (albumId) => dispatch => {
    console.log('fetching picture album id');
    fetch('https://jsonplaceholder.typicode.com/photos?albumId='+albumId)
        .then(res => res.json())
        .then(pictures =>{
            dispatch({
            type: FETCH_PICTURES,
            payload: pictures,
        })});

};

