import { combineReducers } from 'redux';
import albumsReducer from "./albumsReducer";
import picturesReducer from "./picturesReducer";
import wishListReducer from "./wishListReducer";

export default combineReducers( {
    albums: albumsReducer ,
    pictures: picturesReducer ,
    wishesList: wishListReducer
})
