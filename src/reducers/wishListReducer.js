import {ADD_FAVORITE} from "../actions/types";

const initialState = {
    wishes: []
};

export default function (state = initialState, action) {

    switch (action.type) {
        case ADD_FAVORITE:
            return {
                ...state,
                wishes: [...state.wishes, action.payload]
            };
        default:
            return state;
    }
}
