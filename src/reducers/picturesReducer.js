import {FETCH_PICTURES} from "../actions/types";

const initialState = {
    pictures: [],
};

export default function (state = initialState, action) {

    switch (action.type) {
        case FETCH_PICTURES:
            return {
                ...state,
                pictures: action.payload
            };
        default:
            return state;
    }
}
