import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Home from "../components/Home";
import Albums from "../components/Albums";
import WishList from "../components/Wishlist";
import Pictures from "../components/Pictures";
import Picture from "../components/Picture";
import Header from "../components/Header";

class Routes extends Component {
    render() {
        return (
                <Router>
                    <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/catalog" render={() => (
                        <div>
                        <Header />
                            <Route exact path="/catalog/albums" component={Albums} />
                            <Route path="/catalog/wish-list" component={WishList} />
                            <Route path="/catalog/albums/:albumId" component={Pictures} />
                            <Route path="/catalog/pictures/:pictureId" component={Picture} />
                        </div>
                    )} />
                    </Switch>
                </Router>
        );
    }
}

export default Routes;
