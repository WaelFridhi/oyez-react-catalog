import React from 'react';
import { Provider } from 'react-redux';
import store from './store'
import Routes from "./routes/routes";
import ButtonComponent from "./components/ButtonComponent";
import Home from "./components/Home";
import Albums from "./components/Albums";
import Picture from "./components/Picture";
import Header from "./components/Header";

function App() {
  return (
      <Provider store={store}>
          <div className="App">
                  <Routes/>
          </div>
      </Provider>
  );
}

export default App;
