import React, {Component} from 'react';
import Picture from "./Picture";
import {addToFavorite} from "../actions/wishListAction";
import {connect} from "react-redux";

//Style inline button
const buttonContainer = {
    border: '1px solid #5BFE81',
    height: '50px',
    width: '600px',
    backgroundColor: '#5BFE81',
    fontFamily : 'Time new roman',
    fontSize: 'large',
    borderRadius: '10px'
};

class ButtonComponent extends Component {
    constructor (props) {
        super(props);
    }

    render() {
        return (
            <div>
                <button style={buttonContainer} onClick={() => this.props.addToFavorite(this.props.picture)}>Add to wishlist</button>
            </div>
        );
    }
}

export default connect(null , {addToFavorite})(ButtonComponent);
