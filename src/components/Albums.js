import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { fetchAlbums } from "../actions/albumAction";
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

//Style Inline
const albumContainer = {
    width: '200px',
    height: '200px',
    borderRadius: '10px',
    border: '1px solid gray',
    backgroundColor: '#EEEFEE',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '10px',
    margin: '5px'
};
const albumsContainer = {
    display: 'flex',
    justifyContent: 'center',
    padding: '2%',
    flexWrap: 'wrap',
};
// Albums component interface
class Albums extends Component {
    componentWillMount() {
        this.props.fetchAlbums();
    }

    render() {
        const albumsItems = this.props.albums.map(album => (
            <div style={albumContainer} key={album.id}>
                <Link to={`/catalog/albums/${album.id}`}> <p>{album.title}</p></Link>
            </div>
        ));
        return (

                <div style={albumsContainer}>
                    {albumsItems}
                </div>
        );
    }
}

Albums.Prototype = {
    fetchAlbums: PropTypes.func.isRequired,
    albums: PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
    albums : state.albums.items,
});

export default connect(mapStateToProps,
        {fetchAlbums})(Albums);
