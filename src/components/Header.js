import React, {Component} from 'react';
import {Link} from "react-router-dom";

//

class Header extends Component {
    render() {
        return (
            <div>
                <ul>
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/catalog/albums">Catalog</Link></li>
                    <li><Link to="/catalog/wish-list">whish list</Link></li>
                </ul>
            </div>
        );
    }
}

export default Header;
