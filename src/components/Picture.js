import React, {Component} from 'react';
import { connect } from 'react-redux';
import ButtonComponent from "./ButtonComponent";

//Style Inline
const pictureContainer = {
    border: '1px solid black',
    width: '600px',
    height: '600px',
    backgroundColor: '#EEEFEE',
};
const pageContainer = {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column'
};




class Picture extends Component {
    render() {
        console.log(this.props.match.params.pictureId);
        return (
            <div style={pageContainer}>
                <div style={pictureContainer}>
                    <img src={this.props.picture.url} alt=""/>
                </div>
                <ButtonComponent  picture={this.props.picture}/>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => ({
    picture: state.pictures.pictures.find((pic) => parseFloat(pic.id )=== parseFloat(props.match.params.pictureId)),
});

export default connect(mapStateToProps)(Picture);
