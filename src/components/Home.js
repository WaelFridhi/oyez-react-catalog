import React, {Component} from 'react';
import {Link} from "react-router-dom";
import image from '../Oyez_Color.jpg';

//style inline
const homeContainer = {
    height: '100%',
    width: '100%',
    padding: '10%',
    flexWrap: 'center',
    backgroundColor: 'white'
};
const pageContainer = {
    position: 'fixed',
    display: 'flex',
    width: '100%',
    height:'100%',
    textAlign: 'center',
    flexWrap: 'center',
};
const buttonContainer = {
    display: 'inline-block',
    fontWeight: '400',
    position: 'relative',
    color: 'white',
    cursor: 'pointer',
    marginLeft: '10px',
    marginRight: '10px',
    textAlign: 'center',
    verticalAlign: 'middle',
    backgroundColor: '#8BB294',
    border: '1px solid transparent',
    padding: '0.375rem 0.75rem',
    fontSize: '1rem',
    lineHeight: '1.5',
    borderRadius: '0.25rem',
    transition: 'color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out',
};
const imgContainer = {
    width: '300px',
    height: '300px'
};
//Home interface
class Home extends Component {
    render() {
        return (
                    <div style={pageContainer}>
                        <div style={homeContainer}>
                            <img style={imgContainer} src={image}/>
                            <h1>Welcome to Oyez Catalog</h1>
                            <div>
                                <Link to="/catalog/albums">
                                    <button style={buttonContainer}>Catalog</button></Link>
                                <Link to="/catalog/wish-list">
                                    <button style={buttonContainer}>Wish list</button></Link>
                            </div>
                        </div>
                    </div>
        );
    }
}

export default Home;
