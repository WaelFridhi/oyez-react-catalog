import React, {Component} from 'react';
import {connect} from "react-redux";
import {Link} from "react-router-dom";
//Style inline
const imagContainer = {
    width: '150px',
    height: '150px',
    borderRadius: '10px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '10px',
    margin: '5px'
};
const pageContainer = {
    display: 'flex',
    justifyContent: 'center',
    padding: '2%',
    flexWrap: 'wrap',
};

//Wish list interface
class Wishlist extends Component {
    render() {
        console.log(this.props.wishes);
        const wishListItems = this.props.wishes.map(
            (wish) => (
                        <div style={imagContainer} key={wish.id}>
                            <Link  to={`/pictures/${wish.id}`}>
                                <div key={wish.id}>
                                    <img src={wish.thumbnailUrl} alt=""/>
                                </div>
                            </Link>
                        </div>
            ));
        return (
            <div style={pageContainer}>
                {wishListItems}
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    wishes: state.wishesList.wishes
});

export default connect(mapStateToProps)(Wishlist);
