import React, {Component} from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {fetchPictures} from "../actions/picturesAction";
import {Link} from "react-router-dom"
//Style inline
const imagContainer = {
    width: '150px',
    height: '150px',
    borderRadius: '10px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '10px',
    margin: '5px'
};
const pageContainer = {
    display: 'flex',
    justifyContent: 'center',
    padding: '2%',
    flexWrap: 'wrap',
};

//Album's picture
class Pictures extends Component {
    constructor (props) {
        super(props);
    }
    componentWillMount() {
        this.props.fetchPictures(this.props.match.params.albumId);
    }
    render() {
        console.log('***************');
        console.log(this.props.pictures);
        const {pictures} = this.props;
        return (
            <div style={pageContainer}>
                {pictures.map(pic => (
                    <div style={imagContainer}>
                        <Link  to={`/catalog/pictures/${pic.id}`}>
                            <div key={pic.id}>
                                <img src={pic.thumbnailUrl} alt=""/>
                            </div>
                        </Link>
                    </div>
                ))}
            </div>
        );
    }
}

Pictures.Prototype = {
    fetchPictures: PropTypes.func.isRequired,
    pictures: PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
    pictures : state.pictures.pictures,
});

export default connect(mapStateToProps, {fetchPictures})(Pictures);
